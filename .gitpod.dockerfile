FROM gitpod/workspace-full

# upgrade system packages
RUN sudo apt update && \
    sudo apt upgrade -y

# install neovim dependencies and build from source for vscode extension
RUN sudo apt install -y \
    ninja-build \
    gettext \
    libtool \
    libtool-bin \
    autoconf \
    automake \
    cmake \
    g++ \
    pkg-config \
    unzip \
    curl \
    doxygen

RUN git clone https://github.com/neovim/neovim && \
    cd neovim && \
    git checkout stable && \
    make && \
    sudo make install && \
    cd .. && \
    sudo rm -rf neovim

# install aws cli, remove install files, install cdk
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    sudo ./aws/install --update && \
    rm -rf ./aws/ awscliv2.zip && \
    npm i -g aws-cdk@2.25.0 && \
    sudo rm -rf /var/lib/apt/lists/*
